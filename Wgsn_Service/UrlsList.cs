﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wgsn_Service
{
    public class UrlsList
    {
        public string Url { get; set; }
        public DateTime ReceivedDate { get; set; }
    }
}