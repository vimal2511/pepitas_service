﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net.Mail;
using System.IO;
using System.Reflection;
using OfficeOpenXml;


namespace pepitas_Email
{
    internal class EAL
    {
        public string sheetname = "Pepitas_Report_" + DateTime.Now.ToString("yyyyMMddhhmm") + ".xlsx";

        public void updateprojectsize_status()
        {
            BAL bl = new BAL();
            bl.updatestatus();
        }

        public void Send_Email(int type)
        {
            BAL bl = new BAL();
            if (bl.checksend_status(type))
            {
                Send_Email_Process();
            }
        }

        public void Send_Email_Process()
        {
            BAL bl = new BAL();
            try
            {
                List<Sp_getmaster_details_Result> list = new List<Sp_getmaster_details_Result>();
                DataSet dt = bl.Getdailyreport();
                list = (from DataRow dr in dt.Tables[0].Rows
                        select new Sp_getmaster_details_Result()
                        {
                            // Id = Convert.ToInt32(dr["Id"].ToString()),
                            AgentName = Convert.ToString(dr["employee_name"].ToString()),
                            Agentworkeddate = Convert.ToDateTime(dr["assigned_time"].ToString()),
                            Retailer = Convert.ToString(dr["Retailer"].ToString()),
                            regionname = Convert.ToString(dr["regionname"].ToString()),
                            Market_Dept = Convert.ToInt32(dr["Market/Dept"].ToString()),
                            Category = Convert.ToInt32((dr["Category"]).ToString()),
                            SubCategory = Convert.ToInt32(dr["SubCategory"].ToString()),
                            Taxonomy = Convert.ToInt32(dr["Taxonomy"].ToString()),
                            Brands = Convert.ToInt32(dr["Brands"].ToString()),
                            Materials = Convert.ToInt32(dr["Materials"].ToString()),
                            P_G = Convert.ToInt32(dr["P_G"].ToString()),
                            Colors = Convert.ToInt32(dr["Colors"].ToString()),
                            Sizes = Convert.ToInt32(dr["Sizes"].ToString()),
                            OOS = Convert.ToInt32(dr["OOS"].ToString()),
                            Total = Convert.ToInt32(dr["Total"].ToString()),
                            StartTime = Convert.ToString(dr["StartTime"].ToString()),
                            EndTime = Convert.ToString(dr["EndTime"].ToString()),
                            Duriation = Convert.ToString(dr["Duriation"].ToString()),
                            Analyst = Convert.ToString(dr["Analyst"].ToString()),
                            Remarks = Convert.ToString(dr["Remarks"].ToString()),
                            BatchDate = Convert.ToDateTime(dr["BatchDate"].ToString()),
                            //UserId = Convert.ToInt32(dr["UserId"].ToString()),
                            //Deleted = Convert.ToBoolean(dr["Deleted"].ToString()),
                            //regionname = Convert.ToInt32(dr["region"].ToString()) == 1 ? "UK" : (Convert.ToInt32(dr["region"].ToString()) == 2 ? "US" : (Convert.ToInt32(dr["region"].ToString()) == 3 ? "DE" : (Convert.ToInt32(dr["region"].ToString()) == 4 ? "ES" : (Convert.ToInt32(dr["region"].ToString()) == 5 ? "AU" : (Convert.ToInt32(dr["region"].ToString()) == 6 ? "IE" : (Convert.ToInt32(dr["region"].ToString()) == 7 ? "ZA" : "")))))),
                        }).ToList();

                using (ExcelPackage pack = new ExcelPackage())
                {
                    ExcelWorksheet ws = pack.Workbook.Worksheets.Add("Pepitas_Report_" + DateTime.Now.ToString("yyyy-MM-dd"));
                    ws.Cells["A1"].LoadFromCollection(list, true);

                    pack.SaveAs(new FileInfo(sheetname));
                    pack.Dispose();
                }
                DataTable db = ToDataTable(list);
                email_send(db);
            }
            catch (Exception e)
            {
            }
        }

        public void email_send(DataTable list)
        {
            try
            {
                string senderID = ConfigurationSettings.AppSettings["username"].ToString();
                string senderPassword = ConfigurationSettings.AppSettings["password"].ToString();
                string smtp = ConfigurationSettings.AppSettings["smtp"].ToString();
                var tomailid = ConfigurationSettings.AppSettings["tomailid"].ToString();
                var tomailidtemp = ConfigurationSettings.AppSettings["tomailidtemp"].ToString();//temp
                var devemail = ConfigurationSettings.AppSettings["devemail"].ToString();
                var ccmailid1 = ConfigurationSettings.AppSettings["ccmailid1"].ToString();
                var ccmailid2 = ConfigurationSettings.AppSettings["ccmailid2"].ToString();
                var ccmailid3 = ConfigurationSettings.AppSettings["ccmailid3"].ToString();

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(smtp);
                mail.From = new MailAddress(senderID);
                //mail.To.Add(tomailidtemp);
                mail.To.Add(tomailid);
                mail.CC.Add(ccmailid1);
                mail.CC.Add(ccmailid2);
               // if(ccmailid3!="")
              // mail.CC.Add(ccmailid3);
                mail.Bcc.Add(devemail);
                mail.Subject = "Daily Report " + DateTime.Now.ToString("yyyy-MM-dd");
                mail.Body = "Hi," + bodytable(list);
                mail.IsBodyHtml = true;
                if (list.Rows.Count > 0)
                {
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(sheetname);
                    mail.Attachments.Add(attachment);
                }
                SmtpServer.Port = 587;
                SmtpServer.Timeout = 90000;
                SmtpServer.Credentials = new System.Net.NetworkCredential(senderID, senderPassword);
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                mail.Dispose();
                File.Delete(sheetname);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error :" + ex.Message);
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        public string bodytable(DataTable dt)
        {
            StringBuilder bodytable = new StringBuilder();

            bodytable.Append("<!DOCTYPE html><html><body style='background-color: #fff; margin: 0; padding: 0; font-family: 'Oxygen', sans-serif; color: #444; font-size: 15px; line-height: 18px; font-weight: 300;'><article style='margin: 40px 30px;'>");
            if (dt.Rows.Count > 0)
            {
                bodytable.Append("<table class='vitamins' style='margin: 20px 0 0 0; border-collapse: collapse; border-spacing: 0; background: #212121; color: #fff; '><thead style='line-height: 12px; background: #2e63e7; text-transform: uppercase;'><tr>");
                foreach (DataColumn column in dt.Columns)
                {
                    bodytable.Append("<th style='text-align: center;color: #fff; padding: 10px; letter-spacing: 1px; vertical-align: bottom;'>" + column.ColumnName + "</th>");
                }
                bodytable.Append("</tr></thead><tbody style='font-size: 1em; line-height: 15px;'>");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bodytable.Append("<tr style='border-top: 2px solid rgba(109, 176, 231, 0.8); transition: background 0.6s, color 0.6s;'>");
                    var rowval = "";
                    int m = 0;
                    foreach (DataColumn column in dt.Columns)
                    {
                        if (m == 0)
                            rowval += "<td style='padding: 5px;text-align: left; padding-left: 20px; font-weight: 700; background: rgba(109, 176, 231, 0.35); transition: backgrounf 0.6s;'>" + (dt.Rows[i][column.ColumnName].ToString().Replace("00:00:00", "")).Replace("/", "-") + "</td>";
                        else
                            rowval += "<td style='padding: 5px;' onMouseOver='this.style.color=#b92b27' onMouseOut='this.style.color=#00F'>" + (dt.Rows[i][column.ColumnName].ToString().Replace("00:00:00", "")).Replace("/", "-") + "</td>";
                    }
                    bodytable.Append(rowval + "</tr>");// body
                }
                bodytable.Append(" </tbody> </table>");
            }
            else
            {
                bodytable.Append("<p style='font-size:20px !important; background-color: #f0ad4e; display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center;  white-space: nowrap; vertical-align: baseline; padding: 0.2em 0.6em 0.3em; border-radius: 1em;'>No Records found !</p>");// body
            }
            bodytable.Append("</article>");
            bodytable.Append("</body></html>");
            string body = Convert.ToString(bodytable);
            return body;
        }
    }
}