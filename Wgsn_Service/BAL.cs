﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pepitas_Email
{
    internal class BAL
    {
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");

        public DataSet Getdailyreport()
        {
            DataSet ds = new DataSet();
            DAL GetEntityid = new DAL("ConnectionString");
            try
            {
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                var currentdate = indianTime.ToString("yyyy-MM-dd");
                //GetEntityid.AddParameter("@CreatedDate", currentdate);
                //GetEntityid.AddParameter("@UserId", 0);
                //ds = GetEntityid.ExecuteDataSet("Sp_getmaster_details");
                //ds = GetEntityid.ExecuteDataSet("Sp_getReport_details"); testing
                var query = "SELECT u.employee_name, Retailer, [Market/Dept], Category, SubCategory, Taxonomy, Brands, Materials, [P&G] as P_G, Colors, Sizes, OOS, Total, StartTime, EndTime, Duriation, Analyst, Remarks, BatchDate, UserId, Deleted, region,filename, status, weightage, assigned_time,CONVERT(DATE, batchdate, 103) AS batch_date, r.region_code AS regionname FROM [dbo].[pip_masterdetails]  p with(nolock) join[dbo].[region_list] ";
                query += " r with(nolock)  on p.region=r.region_id join [dbo].[user_master] u with(nolock)on u.user_id = p.UserId WHERE  convert(date, assigned_time) = '" + currentdate+"'  AND Deleted = 0 and status !=0 ORDER BY ID ";
                GetEntityid.CommandText = query;
                GetEntityid.commandType = CommandType.Text;
                ds = GetEntityid.ExecuteDataSet();
            }
            catch (Exception e)
            {
                string Errorstring = "Get Daily Report : " + Convert.ToString(e);
            }
            return ds;
        }

        public bool checksend_status(int type)
        {
            DataSet ds = new DataSet();
            DAL GetEntityid = new DAL("testcon");
            try
            {
                GetEntityid.CommandText = "select top 1* from email_status order by 1 desc";
                GetEntityid.commandType = CommandType.Text;
                ds = GetEntityid.ExecuteDataSet();
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                var currenttime = indianTime.Date;
                var m = 0;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var dbcurrent_date = Convert.ToDateTime(ds.Tables[0].Rows[0]["currentdate"].ToString());
                    var email1 = Convert.ToInt32(ds.Tables[0].Rows[0]["email1"]);
                    var email2 = Convert.ToInt32(ds.Tables[0].Rows[0]["email2"]);
                    if (currenttime == dbcurrent_date)
                    {
                        if (email1 == 1 && email2 == 1)
                        {
                            return false;
                        }
                        else if (email1 == 1 && email2 == 0 && type == 2)
                        {
                            DAL dblive = new DAL("testcon");
                            String query = "update email_status set email2 = 1;";
                            dblive.commandType = CommandType.Text;
                            dblive.CommandText = query;
                            dblive.ExecuteDataSet();
                            return true;
                        }
                        else if (email1 == 1 && email2 == 0 && type == 1)
                        { return false; }

                    }
                    else
                    {
                        m = 1;
                    }
                }
                else
                {
                    m = 1;
                }
                if (m == 1)
                {
                    DAL dblive = new DAL("testcon");
                    String query = "insert into email_status(currentdate,email1) values(@currenttime,1)";
                    dblive.commandType = CommandType.Text;
                    dblive.CommandText = query;
                    dblive.AddParameter("@currenttime", currenttime);
                    dblive.ExecuteDataSet();
                    //
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
                //string Errorstring = "Get time span" + Convert.ToString(e);
            }
            return true;
        }

        public void updatestatus()
        {
            DataSet ds = new DataSet();
            DAL GetEntityid = new DAL("ConnectionString");
            try
            {
                GetEntityid.CommandText = "select top 1 Created_date from Mapping_Data order by Created_date desc";
                GetEntityid.commandType = CommandType.Text;
                ds = GetEntityid.ExecuteDataSet();
                var lastdate = (ds.Tables[0].Rows[0][0].ToString().Split(' ')[0]).Split('/');
                var value = lastdate[2] + '-' + lastdate[1] + '-' + lastdate[0];
                GetEntityid.CommandText = "update Mapping_Data set small_count=0,medium_count=0,big_count=0,verybig_count=0 WHERE Created_date='" + value + "'" ;

                GetEntityid.commandType = CommandType.Text;
                GetEntityid.ExecuteDataSet();
            }
            catch (Exception e)
            {
                string Errorstring = "Get Daily Report : " + Convert.ToString(e);
            }
        }
    }
}