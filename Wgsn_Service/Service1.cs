﻿using ActiveUp.Net.Mail;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using pepitas_Email;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;

namespace Wgsn_Service
{
    public partial class Pepitas_service : ServiceBase
    {
        private System.Timers.Timer timer1 = null;
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");

        public Pepitas_service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Library.WriteErrorLog("service STARTED");
            timer1 = new System.Timers.Timer();
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_tick);
            timer1.Interval = 90000;
            timer1.Enabled = true;
        }

        protected override void OnStop()
        {
            timer1.Enabled = false;
            Library.WriteErrorLog("service ENDED");
        }

        private void timer1_tick(object sender, ElapsedEventArgs e)
        {
            Library.WriteErrorLog("service STARTED");
            try
            {
                timer1.Enabled = false;
                List<UrlsList> Urls = ReadGmail();
                ReadContent(Urls);
                timer1.Enabled = true;
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(DateTime.Now.ToString() + ":" + "ERRORS:" + ex.Message);
                timer1.Enabled = true;
            }
            finally
            {
                timer1.Enabled = true;
            }
        }

        public void email_updatestatus()
        {
            EAL el = new EAL();
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            TimeSpan currentTime = indianTime.TimeOfDay;
            TimeSpan currentTime1am = new TimeSpan(02, 00, 00);
            TimeSpan currentTime2pm = new TimeSpan(14, 00, 00);
            TimeSpan currentTime230pm = new TimeSpan(15, 30, 00);

            TimeSpan currentTime23pm = new TimeSpan(22, 00, 00);
            TimeSpan currentTime2330pm = new TimeSpan(23, 59, 00);
            if (currentTime1am > currentTime)  // Time interval for 12 am to 12.20 am
            {
                el.updateprojectsize_status();
            }
            int current_hr = Convert.ToInt32(currentTime.Hours.ToString());
            if (currentTime > currentTime2pm && currentTime < currentTime230pm)  // Time interval for 2 pm to 2:30 pm
            {
                el.Send_Email(1);
            }
            if (currentTime > currentTime23pm && currentTime < currentTime2330pm)  // Time interval for 11 pm to 11:30 pm
            {
                el.Send_Email(2);
            }
           // Driver.Quit();//this line will remove when email send througth service
        }

        private IWebDriver Driver = new ChromeDriver();

        public void ReadContent(List<UrlsList> Urls)
        {
            try
            {
                string BatchDate = "";
                if (Urls.Count() > 0)
                {
                    Library.WriteErrorLog("READ THE CONTENT");
                    ChromeDriverService service = ChromeDriverService.CreateDefaultService();
                    service.HideCommandPromptWindow = true;

                    var options = new ChromeOptions();
                    //options.AddArgument("--window-position=-32000,-32000");
                    options.AddArgument("headless");

                    //Initialize webdriver
                    //IWebDriver Driver = new FirefoxDriver();
                    //IWebDriver Driver = new ChromeDriver();
                    //IWebDriver Driver = new ChromeDriver(service, options);
                    //Navigate by url to website
                    Driver.Navigate().GoToUrl("https://instock-race.wgsn.com/auth/login");
                    //Find username field
                    IWebElement username = Driver.FindElement(By.Name("username"));
                    //Set "username text"
                    string userName = System.Configuration.ConfigurationSettings.AppSettings["LoginName"].ToString();
                    username.SendKeys(userName);

                    IWebElement password = Driver.FindElement(By.Name("password"));
                    //Set "username text"
                    password.SendKeys(System.Configuration.ConfigurationSettings.AppSettings["LoginPassword"].ToString());

                    //Find "Login button"
                    IWebElement LoginButton = Driver.FindElement(By.ClassName("std_grey_button"));
                    //Click by "search button"
                    LoginButton.Click();

                    Library.WriteErrorLog("WGSN LOGIN COMPLETED");
                    //IWebElement Dailyworkflow = Driver.FindElement(By.ClassName("title"));
                    //Dailyworkflow.Click();
                    foreach (UrlsList item in Urls)
                    {
                        string Url = item.Url;
                        var ReceivedDate = String.Format("{0:yyyy-MM-dd}", item.ReceivedDate);
                        Driver.Navigate().GoToUrl(Url); //Redirect to particular page

                        //WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(30));
                        WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromMinutes(10));
                        // bool isElementDisplayed = Driver.FindElement(By.XPath("browse_batches")).Displayed;
                        By @by = By.Name("region");
                        new WebDriverWait(Driver, TimeSpan.FromSeconds(60)).Until(ExpectedConditions.ElementExists(@by));
                        List<IWebElement> elementList = new List<IWebElement>();
                        elementList.AddRange(Driver.FindElements(By.XPath("//div[@class='no-batches-error']")));

                        if (elementList.Count == 0)
                        {
                            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("browse_batches")));

                            if (Driver.FindElement(By.XPath("//table[@id='browse_batches']/tbody/tr")).Displayed)
                            {
                                Library.WriteErrorLog("READ THE CONTENT");
                                IList<IWebElement> ElementCollectionBody = Driver.FindElements(By.XPath("//table[@id='browse_batches']/tbody/tr"));
                                IWebElement created = Driver.FindElement(By.ClassName("datepicker"));

                                string Region = Driver.FindElement(By.Name("region")).GetAttribute("value");
                                string Retailer = Driver.FindElement(By.Name("retailer")).GetAttribute("value");

                                //string Region = Driver.FindElement(By.ClassName("filter-region")).FindElements(By.ClassName("ui-autocomplete-input"))[0].GetAttribute("value");
                                //string Retailer = Driver.FindElement(By.ClassName("filter-retailer")).FindElements(By.ClassName("ui-autocomplete-input"))[0].GetAttribute("value");

                                string CreatedDate = Driver.FindElement(By.ClassName("filter-date")).FindElements(By.ClassName("datepicker"))[0].GetAttribute("value");
                                int SubCategory = 0;
                                int Category = 0;
                                int Department = 0;
                                int Entities = 0;
                                int Post = 0;
                                int Brands = 0;
                                int Prints = 0;
                                int Materials = 0;
                                int Live = 0;

                                int LiveTemp = 0;
                                int total_product = 0;
                                bool flag = false;
                                bool tempflag = false;
                                string Retailer_Name = "";
                                int i = 0;
                                int Colors = 0;

                                int idx = Url.LastIndexOf('/');
                                BatchDate = Url.Substring(idx + 1).Trim();
                                string connectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"].ToString();


                                int wFlowCategory = 0;
                                int wFlowSubCategory = 0;
                                int WFlowMarketorDepartment = 0;
                                int wEntities = 0;
                                int wBrands = 0;
                                int wPrints = 0;
                                int wMaterials = 0;
                                int wColors = 0;
                                foreach (IWebElement row in ElementCollectionBody)
                                {
                                    i++;
                                    if (row.FindElement(By.XPath("td")).GetAttribute("colspan") != "7")
                                    {
                                        Attribute_Inflow Pip = new Attribute_Inflow();
                                        Pip.CreatedDate = Convert.ToDateTime(CreatedDate);
                                        var BatchName = row.FindElement(By.XPath("td[1]")).Text;
                                        if (ElementCollectionBody.Count > i && i != 1)
                                        {
                                            var NextBatch = ElementCollectionBody[i].FindElement(By.XPath("td[1]")).Text;

                                            if (NextBatch != "")
                                            {
                                                flag = true;
                                            }
                                        }
                                        var splitval = BatchName.Split('-');
                                        string curryear = DateTime.Now.Year.ToString();
                                        if (BatchName != "")
                                        {
                                            var val = Regex.Match(BatchName.Split('-')[splitval.Length - 1], @"\d+").ToString();
                                            total_product = Convert.ToInt32(val);
                                        }
                                        if (!tempflag)
                                        {
                                            if (BatchName.Contains(curryear))
                                            {
                                                //Retailer_Name= BatchName.Split(curryear)[0].ToString();
                                                string[] arr = BatchName.Split(new string[] { curryear }, StringSplitOptions.None);
                                                Retailer_Name = arr[0].Remove(arr[0].Length - 1).ToString();
                                            }
                                            else
                                                Retailer_Name = BatchName.Split('-')[0].ToString();

                                            tempflag = true;
                                        }
                                        
                                        var DepartmentTxt = Regex.Match(row.FindElement(By.XPath("td[2]")).Text, @"\d+").Value;
                                        if (!string.IsNullOrEmpty(DepartmentTxt))
                                        {
                                            Department = Department + Convert.ToInt32(DepartmentTxt);
                                            WFlowMarketorDepartment = WFlowMarketorDepartment+Convert.ToInt32(DepartmentTxt);
                                        }

                                        var CategoryTxt = Regex.Match(row.FindElement(By.XPath("td[3]")).Text, @"\d+").Value;
                                        if (!string.IsNullOrEmpty(CategoryTxt))
                                        {
                                            Category = Category + Convert.ToInt32(CategoryTxt);
                                            wFlowCategory = wFlowCategory+Convert.ToInt32(CategoryTxt);
                                        }
                                        var SubCategoryTxt = Regex.Match(row.FindElement(By.XPath("td[4]")).Text, @"\d+").Value;

                                        if (!string.IsNullOrEmpty(SubCategoryTxt))
                                        {
                                            SubCategory = SubCategory + Convert.ToInt32(SubCategoryTxt);
                                            wFlowSubCategory = wFlowSubCategory+Convert.ToInt32(SubCategoryTxt);
                                        }

                                        var ProductEntitiesTxt = Regex.Match(row.FindElement(By.XPath("td[5]")).Text, @"\d+").Value;
                                        var PTxt = row.FindElement(By.XPath("td[5]"));
                                        if (PTxt != null && PTxt.Text != "")
                                        {
                                            if (PTxt.FindElement(By.ClassName("work-link")).GetAttribute("href").Contains("brand"))
                                            {
                                                var BrandTxt = Regex.Match(PTxt.FindElement(By.ClassName("work-link")).Text, @"\d+").Value;
                                                if (!string.IsNullOrEmpty(BrandTxt))
                                                {
                                                    Brands = Brands + Convert.ToInt32(BrandTxt);
                                                    wBrands = wBrands + Convert.ToInt32(BrandTxt);
                                                }
                                            }
                                            if (PTxt.FindElement(By.ClassName("work-link")).GetAttribute("href").Contains("material"))
                                            {
                                                var MaterialTxt = Regex.Match(PTxt.FindElement(By.ClassName("work-link")).Text, @"\d+").Value;
                                                if (!string.IsNullOrEmpty(MaterialTxt))
                                                {
                                                    Materials = Materials + Convert.ToInt32(MaterialTxt);
                                                    wMaterials = wMaterials + Convert.ToInt32(MaterialTxt);
                                                }
                                            }
                                            if (PTxt.FindElement(By.ClassName("work-link")).GetAttribute("href").Contains("print"))
                                            {
                                                var PrintTxt = Regex.Match(PTxt.FindElement(By.ClassName("work-link")).Text, @"\d+").Value;
                                                if (!string.IsNullOrEmpty(PrintTxt))
                                                {
                                                    Prints = Prints + Convert.ToInt32(PrintTxt);
                                                    wPrints = wPrints + Convert.ToInt32(PrintTxt);
                                                }
                                            }
                                            //if (PTxt.FindElement(By.ClassName("work-link")).GetAttribute("href").Contains("color"))
                                            //{
                                            //    var ColorTxt = Regex.Match(PTxt.FindElement(By.ClassName("work-link")).Text, @"\d+").Value;
                                            //    if (!string.IsNullOrEmpty(ColorTxt))
                                            //        Colors = Colors + Convert.ToInt32(ColorTxt);
                                            //}
                                        }

                                        var SKUEntitiesTxt = Regex.Match(row.FindElement(By.XPath("td[6]")).Text, @"\d+").Value;
                                        var SKUTxt = row.FindElement(By.XPath("td[6]"));
                                        if (SKUTxt != null && SKUTxt.Text != "")
                                        {
                                            if (SKUTxt.FindElement(By.ClassName("work-link")).GetAttribute("href").Contains("color"))
                                            {
                                                var ColorTxt = Regex.Match(SKUTxt.FindElement(By.ClassName("work-link")).Text, @"\d+").Value;
                                                if (!string.IsNullOrEmpty(ColorTxt))
                                                {
                                                    Colors = Colors + Convert.ToInt32(ColorTxt);
                                                    wColors = wColors + Convert.ToInt32(ColorTxt);
                                                }
                                            }
                                            if (SKUTxt.FindElement(By.ClassName("work-link")).GetAttribute("href").Contains("size"))
                                            {
                                                var SizeTxt = Regex.Match(SKUTxt.FindElement(By.ClassName("work-link")).Text, @"\d+").Value;
                                                if (!string.IsNullOrEmpty(SizeTxt))
                                                {
                                                    Colors = Colors + Convert.ToInt32(SizeTxt);
                                                    wColors = wColors + Convert.ToInt32(SizeTxt);
                                                }
                                            }
                                        }

                                        if (!string.IsNullOrEmpty(ProductEntitiesTxt))
                                        {
                                            Entities = Entities + Convert.ToInt32(ProductEntitiesTxt);
                                            wEntities = wEntities + Convert.ToInt32(ProductEntitiesTxt);
                                        }
                                            

                                        var Posttxt = Regex.Match(row.FindElement(By.XPath("td[7]")).Text, @"\d+").Value;
                                        if (!string.IsNullOrEmpty(Posttxt))
                                        {
                                            if (row.FindElement(By.XPath("td[7]")).Text.Contains("live"))
                                            {
                                                Live = Live + Convert.ToInt32(Posttxt);
                                                LiveTemp = LiveTemp + Convert.ToInt32(Posttxt);
                                            }
                                            if (row.FindElement(By.XPath("td[7]")).Text.Contains("ready to post"))
                                            {
                                                Post = Post + Convert.ToInt32(Posttxt);
                                                LiveTemp = LiveTemp + Convert.ToInt32(Posttxt);
                                            }
                                        }

                                        if (flag)
                                        {
                                            Retailer_Name = Retailer_Name.Trim();
                                            int weight = total_product - LiveTemp;
                                            using (SqlConnection connection = new SqlConnection(connectionString))
                                            using (SqlCommand cmd = connection.CreateCommand())
                                            {
                                                cmd.CommandText = "USP_INSERT_Pepitas_data";
                                                cmd.Parameters.AddWithValue("@batchdate", BatchDate);
                                                cmd.Parameters.AddWithValue("@region", Region);
                                                cmd.Parameters.AddWithValue("@retailername", Retailer_Name);
                                                cmd.Parameters.AddWithValue("@weightage", weight);
                                                cmd.Parameters.AddWithValue("@createddate", ReceivedDate);

                                                int wrflow_Total = (WFlowMarketorDepartment * 3) + (wFlowCategory * 2) + wFlowSubCategory;
                                                wrflow_Total = wrflow_Total + wEntities + wBrands + wPrints + wMaterials + wColors;
                                                string wrflow_Value= string.Format("({0})*3+ ({1})*2+({2})+({3})+({4})+({5})+({6})+({7})", WFlowMarketorDepartment, wFlowCategory, wFlowSubCategory, wEntities, wBrands, wPrints, wMaterials, wColors);
                                                cmd.Parameters.AddWithValue("@wrflow_Total", wrflow_Total);
                                                cmd.Parameters.AddWithValue("@wrflow_Value", wrflow_Value);
                                                cmd.CommandType = CommandType.StoredProcedure;
                                                connection.Open();
                                                cmd.ExecuteNonQuery();
                                                connection.Close();
                                            }
                                            LiveTemp = 0;
                                            flag = false;
                                            tempflag = false;

                                            wFlowCategory = 0;
                                            wFlowSubCategory = 0;
                                            WFlowMarketorDepartment = 0;
                                            wEntities = 0;
                                            wBrands = 0;
                                            wPrints = 0;
                                            wMaterials = 0;
                                            wColors = 0;
                                        }
                                    }
                                }

                                DataTable dt = new DataTable();
                                using (SqlConnection connection = new SqlConnection(connectionString))
                                using (SqlCommand cmd = connection.CreateCommand())
                                {
                                    cmd.CommandText = "Save_Pip_MasterDetails";
                                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                                    cmd.Parameters.AddWithValue("@BatchDate", BatchDate);
                                    cmd.Parameters.AddWithValue("@Dept_Market", Department);
                                    cmd.Parameters.AddWithValue("@Region", Region);
                                    cmd.Parameters.AddWithValue("@Category", Category);
                                    cmd.Parameters.AddWithValue("@SubCategory", SubCategory);
                                    cmd.Parameters.AddWithValue("@Brands", Brands);
                                    cmd.Parameters.AddWithValue("@Prints", Prints);
                                    cmd.Parameters.AddWithValue("@Materials", Materials);
                                    cmd.Parameters.AddWithValue("@Colors", Colors);
                                    cmd.Parameters.AddWithValue("@ReadyToPost", Post);
                                    cmd.Parameters.AddWithValue("@Live", Live);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    connection.Open();
                                    cmd.CommandTimeout = 300;
                                    cmd.ExecuteNonQuery();
                                    connection.Close();
                                    Library.WriteErrorLog("INSERT THE DATA INTO DATABASE");
                                }
                            }
                        }//
                    }
                    Library.WriteErrorLog("COMPLETED");
                    Driver.Quit();
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog("Function Name : ReadContent -- Error : " + ex.Message);
            }
        }

        private bool IsElementPresent(By by)
        {
            try
            {
                Driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public List<UrlsList> ReadGmail()
        {
            email_updatestatus();
            List<UrlsList> Urls = new List<UrlsList>();
            try
            {
                Library.WriteErrorLog("READ GMAILS STARTED");
                string Subject = string.Empty;
                string Batchdate = string.Empty;
                var username = System.Configuration.ConfigurationSettings.AppSettings["UserName"].ToString();
                var password = System.Configuration.ConfigurationSettings.AppSettings["Password"].ToString();
                string Url = string.Empty;
                string Region = string.Empty;

                Imap4Client imap = new Imap4Client();
                imap.ConnectSsl("imap.gmail.com", 993);
                imap.Login(username, password);
                Mailbox inbox = imap.SelectMailbox("inbox");

                var messages = inbox.SearchParse("UNSEEN").Cast<Message>();

                // int sucssrun = 0;
                foreach (Message email in messages)
                {
                    var Summar = email.BodyText.Text;
                    Subject = email.Subject;
                    if (Subject.Contains(System.Configuration.ConfigurationSettings.AppSettings["Subject"].ToString()))
                    {
                        var doc = new HtmlDocument();
                        doc.LoadHtml(email.BodyHtml.Text);

                        if (!string.IsNullOrEmpty(Subject))
                        {
                            int index = Subject.IndexOf('-');
                            DateTime received_date = Convert.ToDateTime(email.ReceivedDate); // Convert.ToDateTime();//Received.Text;

                            string first = Subject.Substring(0, index);
                            string second = Subject.Substring(index + 1);
                            int Lastindex = second.LastIndexOf('-');
                            Batchdate = second.Substring(0, Lastindex).Trim();
                            int idx = Subject.LastIndexOf('-');
                            Region = Subject.Substring(idx + 1).Trim();
                            Url = "https://instock-race.wgsn.com/batches/daily_workflow/" + Region + "/0/" + Batchdate;
                            Library.WriteErrorLog("RETURN ULRS");
                            var match = Urls.Where(x => x.Url.Contains(Url)).FirstOrDefault();
                            if (match == null)
                                Urls.Add(new UrlsList { Url = Url, ReceivedDate = received_date });
                        }
                    }
                    // sucssrun = 1;
                }
                //if (sucssrun == 0)
                //{
                //    var flags = new FlagCollection();
                //    flags.Add("UNSEEN");
                //    for (int i = 0; i < messages.Count(); i++)
                //        inbox.AddFlags(messages[0]., flags);
                //}
                //    }
                //}

                #region Poobalan code

                //MailRepository rep = new MailRepository("imap.gmail.com", 993, true, username, password);

                //foreach (Message email in rep.GetUnreadMails("Inbox"))
                //{
                //    //var ss = string.Format("{0}: {1}{2}", email.From, email.Subject, email.BodyHtml.Text);
                //    var Summar = email.BodyText.Text;
                //    Subject = email.Subject;
                //    //if (Summar.Contains(System.Configuration.ConfigurationSettings.AppSettings["Summary"].ToString()) && Subject.Contains(System.Configuration.ConfigurationSettings.AppSettings["Subject"].ToString()))
                //    if (Subject.Contains(System.Configuration.ConfigurationSettings.AppSettings["Subject"].ToString()))
                //    {
                //        var doc = new HtmlDocument();
                //        doc.LoadHtml(email.BodyHtml.Text);
                //        //var tempValue = doc.DocumentNode.SelectSingleNode("//a");
                //        //var link = tempValue.Attributes["href"].Value;
                //        if (!string.IsNullOrEmpty(Subject))
                //        {
                //            int index = Subject.IndexOf('-');
                //            string first = Subject.Substring(0, index);
                //            string second = Subject.Substring(index + 1);
                //            int Lastindex = second.LastIndexOf('-');
                //            Batchdate = second.Substring(0, Lastindex).Trim();
                //            int idx = Subject.LastIndexOf('-');
                //            Region = Subject.Substring(idx + 1).Trim();
                //            Url = "https://instock-race.wgsn.com/batches/daily_workflow/" + Region + "/0/" + Batchdate;
                //            Library.WriteErrorLog("RETURN ULRS");
                //            Urls.Add(new UrlsList { Url = Url });
                //        }
                //    }
                //     }

                #endregion Poobalan code

                //"https://instock-race.wgsn.com/batches/daily_workflow/us/0/2017-11-08";
                //if (email.Attachments.Count > 0)
                //{
                //    foreach (MimePart attachment in email.Attachments)
                //    {
                //        var sss = string.Format("Attachment: {0} {1}", attachment.ContentName, attachment.ContentType.MimeType);
                //    }
                //}

                Library.WriteErrorLog("NO MAILS");

                if (Urls.Count() == 0)
                    Driver.Quit();
                return Urls;
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog("Function Name : ReadGmail -- Error : " + ex.Message);
                return Urls;
            }
        }
    }
}