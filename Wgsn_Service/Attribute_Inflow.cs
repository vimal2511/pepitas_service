﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wgsn_Service
{
    public class Attribute_Inflow
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> BatchDate { get; set; }
        public string Retailler { get; set; }
        public string Region { get; set; }
        public Nullable<int> Category { get; set; }
        public Nullable<int> SubCategory { get; set; }
        public Nullable<int> Brands { get; set; }
        public Nullable<int> Prints { get; set; }
        public Nullable<int> Materials { get; set; }
        public Nullable<int> Colors_Size { get; set; }
        public Nullable<int> ReadyToPost { get; set; }
        public Nullable<int> Live { get; set; }
    }
}
